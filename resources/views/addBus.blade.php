@extends('layouts.app')

@section('content')

    @include('nav')

    <div class="main-content container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    @if( Session::has('success') )
                        <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
                    @endif


                    @if( Session::has('error') )
                        <div class="alert alert-error" align="center">{{Session::get('error')}}</div>
                    @endif

                    <div class="panel-heading panel-heading-divider">Bus Details<span class="panel-subtitle">Enter Bus Details</span></div>
                    <div class="panel-body">
                        <form method="post" action="{{url('add-bus')}}">
                            {{csrf_field()}}

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Name</label>
                                <div class="col-sm-6">
                                    <input type="text" required="" name="name" placeholder="" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Registration Number</label>
                                <div class="col-sm-6">
                                    <input type="text" required="" name="regno" placeholder="" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Model</label>
                                <div class="col-sm-6">
                                    <input type="text" name="model" required="" placeholder="" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Capacity</label>
                                <div class="col-sm-6">
                                    <input type="text" name="capacity" required="" placeholder="" class="form-control">
                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-sm-2 col-sm-10">
                                    <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                    <button class="btn btn-space btn-secondary">Cancel</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection