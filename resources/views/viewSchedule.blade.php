@extends('layouts.app')

@section('content')

    @include('nav')
    <div class="main-content container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Default
                        <div class="tools"><span class="icon s7-cloud-download"></span><span class="icon s7-edit"></span></div>
                    </div>
                    <div class="panel-body">

                        <table id="table1" class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>Departure Location</th>
                                <th>Departure Date</th>
                                <th>Departure Time</th>
                                <th>Arrival Location</th>
                                <th>Arrival Date</th>
                                <th>Arrival Time</th>
                                <th>Price (GHC)</th>
                                <th>Seats Available</th>
                                <th>Bus</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($schedules as $item)

                                <tr>
                                    <td>{{$item->departureLocation}}</td>
                                    <td>{{$item->departureDate}}</td>
                                    <td>{{$item->departureTime}}</td>
                                    <td>{{$item->arrivalLocation}}</td>
                                    <td>{{$item->arrivalDate}}</td>
                                    <td>{{$item->arrivalTime}}</td>
                                    <td>{{$item->price}}</td>
                                    <td>{{App\seat::where('shid',$item->shid)->where('status','Available')->count()}}</td>
                                    <td>{{$item->Bus->name}} - {{$item->Bus->regno}}</td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection