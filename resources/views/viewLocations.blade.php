@extends('layouts.app')

@section('content')

    @include('nav')
    <div class="main-content container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">CURRENT LOCATIONS
                        <div class="tools"><span class="icon s7-cloud-download"></span><span class="icon s7-edit"></span></div>
                    </div>
                    <div class="panel-body">

                        <table id="table1" class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Name</th>
                                <th>Created On</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $count = 1; ?>
                            @foreach($locations as $item)

                                <tr>
                                    <td>{{$count}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->created_at}}</td>
                                </tr>
                                <?php $count++ ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection