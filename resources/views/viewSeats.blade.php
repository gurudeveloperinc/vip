@extends('layouts.app')

@section('content')

    @include('nav')
    <div class="main-content container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">
                        {{$schedule->Bus->name}} - {{$schedule->Bus->regno}} <br>
                        {{$schedule->departureLocation}} to {{$schedule->arrivalLocation}} <br>
                        Departs- {{$schedule->departureTime}}  | Arrives - {{$schedule->arrivalTime}} <br>
                        Price - GHC {{$schedule->price}} <br>
                        {{$available}} seats available

                        <div class="tools"><span class="icon s7-cloud-download"></span><span class="icon s7-edit"></span></div>
                    </div>
                    <div class="panel-body">
                        <table id="table1" class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th>Seat Number</th>
                                <th>Status</th>
                                <th>Name</th>
                                <th>Trans ID</th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody>

                            @foreach($seats as $item)

                                <tr>
                                    <td>{{$item->seatno}}</td>
                                    <td>
                                        @if($item->status == "Available")

                                        <p class="alert alert-success" style="margin-bottom:0; width: 100px; padding: 10px 20px;">{{$item->status}}</p>

                                        @else

                                        <p class="alert alert-danger" style="margin-bottom:0; width: 100px; padding: 10px 20px;">{{$item->status}}</p>
                                        @endif
                                    </td>

                                    <td>

                                        <p>
                                        <?php
                                            $booking = App\booking::where('sid',$item['sid'])->where('transactionid',"<>", null)->first();
                                            echo $booking['fname'] . " " .  $booking['sname'];
                                        ?>
                                        </p>

                                    </td>

                                    <td>
                                        <p>
                                            <?php
                                            $booking = App\booking::where('sid',$item['sid'])->where('transactionid',"<>", null)->first();
                                            echo $booking['transactionid'];
                                            ?>
                                        </p>

                                    </td>
                                    <td>
                                        @if($item->status == 'Booked')
                                            <a href="{{url('/booking-details/' . $item->sid )}}" class="btn btn-success">VIEW</a>
                                            @if(isset($booking->bid))
                                            <a href="{{url('/remove-booking/' . $booking->bid .'/' . $item->shid )}}" class="btn btn-primary">REMOVE</a>
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection