<html>
<head>
    <title>VIP Payment Page</title>
    <link href="{{url('mobile/css/materialize.min.css')}}" rel="stylesheet">
    <script src="{{url('mobile/js/jquery.js')}}"></script>
    <script src="{{url('mobile/js/materialize.min.js')}}"></script>
</head>
<body>

<div align="center" class="logoDiv">
    <img src="{{url('img/logo.png')}}" alt="Thamani" height="100" class="logo">
</div>

<style>
    .header{
        text-align: center;
    }
    nav{
        background: #F23333;
    }
    body{
        padding-bottom: 30px;
    }
</style>
<nav class="nav-extended">
    <div class="nav-wrapper">
        <a href="#" class="brand-logo">
        </a>

        <h5 class="header">CANCEL TICKET</h5>

        <ul class="tabs tabs-transparent">
            <li class="tab"><a class="active" href="#flights">DETAILS</a></li>

        </ul>
    </div>
</nav>


<div class="main-content container">
    @if( Session::has('success') )
        <div class="success green white-text"  align="center">{{Session::get('success')}}</div>
    @endif

    @if( Session::has('error') )
        <div class="error" align="center">{{Session::get('error')}}</div>
    @endif
    <div class="row">
        <div class="col s12">
            <form method="post" action="{{url('cancel-ticket')}}">

                {{csrf_field()}}
                <div class="row">
                    <div class="input-field col s12">
                        <input  type="text" name="reference" required class="validate">
                        <label >Reference Number</label>
                    </div>
                </div>

                <button type="submit" class="btn btn-teal">CANCEL</button>
            </form>
        </div>
    </div>
</div>


</body>
</html>
