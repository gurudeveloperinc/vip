@extends('layouts.app')

@section('content')

    @include('nav')

    <div class="main-content container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    @if( Session::has('success') )
                        <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
                    @endif


                    @if( Session::has('error') )
                        <div class="alert alert-error" align="center">{{Session::get('error')}}</div>
                    @endif

                    <div class="panel-heading panel-heading-divider">Add New Admin<span class="panel-subtitle">Enter admin details</span></div>
                    <div class="panel-body">
                        <form method="post" action="{{url('add-admin')}}">
                            {{csrf_field()}}



                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Name</label>
                                <div class="col-sm-6">
                                    <input type="text" required="" placeholder="" name="name" class="form-control">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Email</label>
                                <div class="col-sm-6">
                                    <input type="email" required="" name="email" placeholder="" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Password</label>
                                <div class="col-sm-6">
                                    <input type="text" required="" name="password" class="form-control">
                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-sm-12" align="center">
                                    <button type="submit" class="btn btn-space btn-success">Submit</button>
                                    <button class="btn btn-space btn-primary">Cancel</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection