<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/img/favicon.html">
    <title>VIP TICKET BOOKING RESERVATION</title>
    <link rel="stylesheet" type="text/css" href="assets/lib/stroke-7/style.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link type="text/css" href="{{url('assets/css/app.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{url('css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/jquery.timepicker.css')}}">
</head>
<body>
@yield('content')

<footer>
    <p align="center">Copyright 2017. Project work by Rhoda</p>
</footer>


<script src="{{url('js/jquery.js')}}" type="text/javascript"></script>
<script src="{{url('js/jquery.timepicker.min.js')}}"></script>
<script src="{{url('assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}" type="text/javascript"></script>


<script src="{{url('assets/lib/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/js/app.js')}}" type="text/javascript"></script>

<script>
    $(document).ready(function () {
        $('.time').timepicker();
    })
</script>

</body>
</html>