@extends('layouts.app')

@section('content')

    @include('nav')
    <div class="container">
        <div class="jumbotron">
            <img src="{{url('img/pic1.jpg')}}" style="width: 100%; height: 350px;">
        </div>

        <div class="welcome row col-md-12" style="margin-bottom: 50px;" align="center">

            @if(Auth::user()->role == "Admin")
            <div align="center" class="col-md-6">
                <h3>View Schedule</h3>
                <p>
                    To view the list of departures along with their routes
                </p>
                <a class="btn btn-success" href="{{url('view-schedule')}}">View Schedule</a>
            </div>

            <div align="center" class="col-md-6">
                <h3>View Bookings</h3>
                <p>
                    To view the list of bookings made for each schedule
                </p>
                <a class="btn btn-success" href="{{url('view-bookings')}}">View Bookings</a>
            </div>


            @endif

            @if(Auth::user()->role == "System Admin")
                <div class="row col-md-12">
                    <div class="col-md-6">
                        <h3>View Admins</h3>
                        <p>
                            To view the list of admins in the system or add new admins
                        </p>
                        <a class="btn btn-success" href="{{url('view-admins')}}">View Admins</a>

                    </div>
                    <div class="col-md-6">
                        <h3>View Customers</h3>
                        <p>
                            To view the list of customers in the system
                        </p>
                        <a class="btn btn-success" href="{{url('view-customers')}}">View Customers</a>

                    </div>
                </div>
            @endif

        </div>
    </div>

@endsection