@extends('layouts.app')

@section('content')

    @include('nav')
    <div class="main-content container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Reports
                        <div class="tools">All Bookings<span class="icon s7-cloud-download">Total Bookings {{count($bookings)}}</span><span class="icon s7-edit"></span></div>
                    </div>

                    <div>
                        <label>Duration</label>
                        <form action="{{url('view-reports')}}">
                        <select name="days">
                            <option value="7">Last 7 Days</option>
                            <option value="14">Last 14 Days</option>
                            <option value="30">Last 30 Days</option>
                            <option value="60">Last 60 Days</option>
                            <option value="90">Last 90 Days</option>
                        </select>
                            <button>Submit</button>
                        </form>

                        <label>Custom Duration:</label>
                        <form action="{{url('view-reports')}}">
                            <input type="number" name="days">
                            <button>Submit</button>
                        </form>

                        <h3 align="center">
                            @if(\Illuminate\Support\Facades\Input::has('days'))
                                Last {{\Illuminate\Support\Facades\Input::get('days')}} days
                            @else
                                All
                            @endif
                        </h3>
                        {{--<p>Schedule with the highest booking: <a class="btn btn-success" href="{{url('booking-details/' . $highest)}}">View</a> </p>--}}
                    </div>
                    <div class="panel-body">
                        <table id="table1" class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Surname</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Schedule</th>
                                <th>Seat No</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($bookings as $item)

                                <tr>
                                    <td>{{$item->fname}}</td>
                                    <td>{{$item->sname}}</td>
                                    <td>{{$item->email}}</td>
                                    <td>{{$item->phone}}</td>
                                    <td>
                                        {{$item->Seat->Schedule->departureLocation}} to {{$item->Seat->Schedule->arrivalLocation}} <br>
                                        {{$item->Seat->Schedule->departureDate}} | {{$item->Seat->Schedule->departureTime}}
                                    </td>
                                    <td>
                                        {{$item->Seat->seatno}}
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <hr>
                        <br><br>
                        <h3>Locations</h3>
                        <table class="table table-striped">
                            <tr>
                                <th>Name</th>
                                <th>No of departure Schedules</th>
                                <th>No of arrival Schedules</th>
                            </tr>

                            @foreach($locations as $item)

                                <tr>
                                    <td>{{$item->name}}</td>
                                    <td>{{\App\schedule::where('departureLocation',$item->name)->count()}}</td>
                                    <td>{{\App\schedule::where('arrivalLocation',$item->name)->count()}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection