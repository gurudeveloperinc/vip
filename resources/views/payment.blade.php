<html>
<head>
    <title>VIP Payment Page</title>
		<meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <link href="{{url('mobile/css/materialize.min.css')}}" rel="stylesheet">
    <script src="{{url('mobile/js/jquery.js')}}"></script>
    <script src="{{url('mobile/js/materialize.min.js')}}"></script>
</head>
<body>

<div align="center" class="logoDiv">
    <img src="{{url('img/logo.png')}}" alt="Thamani" height="100" class="logo">
</div>

<style>
    .header{
        text-align: center;
    }
    nav{
        background: #F23333;
    }
    body{
        padding-bottom: 30px;
    }
</style>
<nav class="nav-extended">
    <div class="nav-wrapper">
        <a href="#" class="brand-logo">
        </a>

        <h5 class="header">PAYMENTS</h5>

        <ul class="tabs tabs-transparent">
            <li class="tab"><a class="active" href="#flights">DASHBOARD</a></li>

        </ul>
    </div>
</nav>


    <div class="main-content container">
        <div class="row">
            <div class="col s12">
                <div align="center">
                    <div class="flow-text">
                        {{$schedule->Bus->name}} - {{$schedule->Bus->regno}} <br>
                        {{$schedule->departureLocation}} to {{$schedule->arrivalLocation}} <br>
                        Departs- {{$schedule->departureTime}}  | Arrives - {{$schedule->arrivalTime}} <br>

                        <h3 class="green-text">GHC {{$schedule->price}}</h3>
                    </div>
                    <div >

                        <h5 class="sub-title">Payment for seat {{$seat->seatno}} </h5>

                        <form id="paymentForm" method="post" action="{{url('/pay')}}">
                            {{csrf_field()}}

                            <div class="row">
                                <div class="input-field col s6">
                                    <input id="first_name" name="fname" type="text" class="validate">
                                    <label for="first_name">First Name</label>
                                </div>
                                <div class="input-field col s6">
                                    <input id="last_name" name="sname" type="text" class="validate">
                                    <label for="last_name">Surname</label>
                                </div>
                            </div>


                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="email" type="email" name="email" class="validate">
                                    <label for="email">Email</label>
                                </div>
                            </div>


                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="email" type="text" name="phone" class="validate">
                                    <label for="email">Phone</label>
                                </div>
                            </div>

                            <input type="hidden" name="seat" value="{{$seat->sid}}">
                            <input type="hidden" name="reference" value="{{$reference}}">
                            <input type="hidden" name="schedule" value="{{$schedule->shid}}">

                            <!-- Modal Trigger -->
                            <a class="waves-effect waves-light btn" id="pay" href="#modal">Pay</a>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


<!-- Modal Structure -->
<div id="modal" class="modal">
    <div class="modal-content">
        <h4>Payment Details </h4>
        <h4 class="green-text">Amount due GHC {{$schedule->price}}</h4>
        <h4>Your reference - {{$reference}}</h4>
        <p>
            Please pay into AIRTEL mobile money account number - <b>0261332884</b> <br> then go
            to the booking section of the application and insert your reference number and the transaction ID.
            <span class="red-text">*Keep your reference number safe and confidential</span>
        </p>
    </div>
    <div class="modal-footer">
        <a onclick="document.getElementById('paymentForm').submit()"  class="modal-action modal-close waves-effect waves-green btn-flat">Okay</a>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.modal').modal();
    });
</script>


</body>
</html>
