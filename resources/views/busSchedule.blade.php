<html>
<head>
    <title>VIP Payment Page</title>
	<meta name='viewport' content='width=device-width,initial-scale=1.0'>
    <link href="{{url('mobile/css/materialize.min.css')}}" rel="stylesheet">
    <script src="{{url('mobile/js/jquery.js')}}"></script>
    <script src="{{url('mobile/js/materialize.min.js')}}"></script>
</head>
<body>

<div align="center" class="logoDiv">
    <img src="{{url('img/logo.png')}}" alt="Thamani" height="100" class="logo">
</div>

<style>
    .header{
        text-align: center;
    }
    nav{
        background: #F23333;
    }
    body{
        padding-bottom: 30px;
    }
</style>
<nav class="nav-extended">
    <div class="nav-wrapper">
        <a href="#" class="brand-logo">
        </a>

        <h5 class="header">BUS SCHEDULE</h5>

        <ul class="tabs tabs-transparent">
            <li class="tab"><a class="active" href="#flights">DETAILS</a></li>

        </ul>
    </div>
</nav>
@if( Session::has('success') )
    <div class="success"  align="center">{{Session::get('success')}}</div>
@endif

@if( Session::has('error') )
    <div class="error" align="center">{{Session::get('error')}}</div>
@endif

<div class="main-content container">
    <div class="row">
        <div class="col s12">
            <table class="striped centered responsive-table">
                <thead>
                <tr>
                    <th>Departure Location</th>
                    <th>Departure Date</th>
                    <th>Departure Time</th>
                    <th>Arrival Location</th>
                    <th>Arrival Date</th>
                    <th>Arrival Time</th>
                    <th>Price (GHC)</th>
                    <th>Bus</th>
                </tr>
                </thead>
                <tbody>

                @foreach($schedules as $item)

                    <tr>
                        <td>{{$item->departureLocation}}</td>
                        <td>{{$item->departureDate}}</td>
                        <td>{{$item->departureTime}}</td>
                        <td>{{$item->arrivalLocation}}</td>
                        <td>{{$item->arrivalDate}}</td>
                        <td>{{$item->arrivalTime}}</td>
                        <td>{{$item->price}}</td>
                        <td>{{$item->Bus->name}} - {{$item->Bus->regno}}</td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


</body>
</html>
