
<nav class="navbar navbar-full navbar-inverse navbar-fixed-top mai-top-header">
    <div class="container"><a href="#" class="navbar-brand"></a><span style="color: white;
margin-left: -80px;
font-weight: 700;
font-size: 16px;">Bus Transport Service</span>

        <!--User Menu-->
        <ul class="nav navbar-nav float-lg-right mai-user-nav">
            <li class="dropdown nav-item">
                @if(!Auth::guest())
                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false"
                                             class="dropdown-toggle nav-link">
                    <img src="{{url('img/logo.png')}}"><span class="user-name">Logged in as {{Auth::user()->name}}</span><span
                            class="angle-down s7-angle-down"></span>
                </a>

                <div role="menu" class="dropdown-menu">
                    <a href="{{url('logout')}}" class="dropdown-item">
                        <span class="icon s7-power"> </span>Log Out</a>
                </div>

                @endif
            </li>
        </ul>
    </div>
</nav>
<nav class="navbar mai-sub-header">
    <div class="container">
        <!-- Mega Menu structure-->
        <nav class="navbar navbar-toggleable-sm">
            <button type="button" data-toggle="collapse" data-target="#mai-navbar-collapse"
                    aria-controls="#mai-navbar-collapse" aria-expanded="false" aria-label="Toggle navigation"
                    class="navbar-toggler hidden-md-up collapsed">
                <div class="icon-bar"><span></span><span></span><span></span></div>
            </button>
            <div id="mai-navbar-collapse" class="navbar-collapse collapse mai-nav-tabs">
                <ul class="nav navbar-nav">


                    @if(!Auth::guest())

                        <li class="nav-item parent
                            @if($path == 'home' )
                                open
                            @endif
                                "><a href="{{url('/home')}}" role="button" aria-expanded="false"
                                     class="nav-link"><span
                                        class="icon s7-home"></span><span>Home</span></a>

                        </li>


                    @if(Auth::user()->role == "Admin")


                    <li class="nav-item parent
                        @if($path == 'view-schedule' || $path == 'add-schedule' )
                            open
                        @endif
                    "><a href="{{url('view-schedule')}}" role="button" aria-expanded="false" class="nav-link"><span
                                    class="icon s7-rocket"></span><span>Schedule</span></a>
                        <ul class="mai-nav-tabs-sub mai-sub-nav nav">
                            <li class="nav-item"><a href="{{url('add-schedule')}}" class="nav-link"><span
                                            class="name">Add New Schedule</span></a>
                            </li>
                            <li class="nav-item"><a href="{{url('view-schedule')}}" class="nav-link"><span class="name">View Schedule</span></a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-item parent
                        @if($path == 'view-bookings' || \Illuminate\Support\Str::contains($path,'view-booking') )
                            open
                        @endif
                    "><a href="{{url('view-bookings')}}" role="button" aria-expanded="false" class="nav-link"><span
                                    class="icon s7-diamond"></span><span>Bookings</span></a>
                        <ul class="mai-nav-tabs-sub mai-sub-nav nav">

                            <li class="nav-item"><a href="{{url('view-bookings')}}" class="nav-link"><span
                                            class="icon s7-box2"></span><span class="name">View Bookings</span></a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item parent
                        @if($path == 'view-buses' || $path == 'add-bus')
                            open
                        @endif
                    "><a href="{{url('view-buses')}}" role="button" aria-expanded="false" class="nav-link"><span
                                    class="icon s7-tools"></span><span>Buses</span></a>
                        <ul class="mai-nav-tabs-sub mai-sub-nav nav">
                            <li class="nav-item"><a href="{{url('add-bus')}}" class="nav-link"><span class="icon s7-graph"></span><span
                                            class="name">Add New Bus</span></a>
                            </li>
                            <li class="nav-item"><a href="{{url('view-buses')}}" class="nav-link"><span
                                            class="icon s7-graph1"></span><span class="name">View Buses</span></a>
                            </li>

                        </ul>
                    </li>


                        <li class="nav-item parent
                        @if($path == 'view-locations' || $path == 'add-location')
                                open
                            @endif
                                "><a href="{{url('view-locations')}}" role="button" aria-expanded="false" class="nav-link"><span
                                        class="icon s7-tools"></span><span>Locations</span></a>
                            <ul class="mai-nav-tabs-sub mai-sub-nav nav">
                                <li class="nav-item"><a href="{{url('add-location')}}" class="nav-link"><span class="icon s7-graph"></span><span
                                                class="name">Add New Location</span></a>
                                </li>
                                <li class="nav-item"><a href="{{url('view-locations')}}" class="nav-link"><span
                                                class="icon s7-graph1"></span><span class="name">View Location</span></a>
                                </li>

                            </ul>
                        </li>


                            <li class="nav-item parent
                        @if($path == 'view-customers')
                                    open
                                @endif
                                    "><a href="{{url('view-customers')}}" role="button" aria-expanded="false" class="nav-link"><span
                                            class="icon s7-tools"></span><span>Customers</span></a>
                                <ul class="mai-nav-tabs-sub mai-sub-nav nav">

                                    <li class="nav-item"><a href="{{url('view-customers')}}" class="nav-link"><span
                                                    class="icon s7-graph1"></span><span class="name">View Customers</span></a>
                                    </li>

                                </ul>
                            </li>

                            <li class="nav-item parent
                        @if($path == 'view-reports')
                                    open
                                @endif
                                    "><a href="{{url('view-reports')}}" role="button" aria-expanded="false" class="nav-link"><span
                                            class="icon s7-tools"></span><span>Reports</span></a>
                                <ul class="mai-nav-tabs-sub mai-sub-nav nav">

                                    <li class="nav-item"><a href="{{url('view-reports')}}" class="nav-link"><span
                                                    class="icon s7-graph1"></span><span class="name">View Reports</span></a>
                                    </li>

                                </ul>
                            </li>


                        @else

                            <li class="nav-item parent
                        @if($path == 'view-admins' || $path == 'add-admin')
                                    open
                                @endif
                                    "><a href="{{url('view-admins')}}" role="button" aria-expanded="false" class="nav-link"><span
                                            class="icon s7-tools"></span><span>Admins</span></a>
                                <ul class="mai-nav-tabs-sub mai-sub-nav nav">
                                    <li class="nav-item"><a href="{{url('add-admin')}}" class="nav-link"><span class="icon s7-graph"></span><span
                                                    class="name">Add New Admin</span></a>
                                    </li>
                                    <li class="nav-item"><a href="{{url('view-admins')}}" class="nav-link"><span
                                                    class="icon s7-graph1"></span><span class="name">View Admins</span></a>
                                    </li>

                                </ul>
                            </li>

                            <li class="nav-item parent
                        @if($path == 'view-customers')
                                    open
                                @endif
                                    "><a href="{{url('view-customers')}}" role="button" aria-expanded="false" class="nav-link"><span
                                            class="icon s7-tools"></span><span>Customers</span></a>
                                <ul class="mai-nav-tabs-sub mai-sub-nav nav">

                                    <li class="nav-item"><a href="{{url('view-customers')}}" class="nav-link"><span
                                                    class="icon s7-graph1"></span><span class="name">View Customers</span></a>
                                    </li>

                                </ul>
                            </li>




                        @endif
                    @endif
                </ul>
            </div>
        </nav>

    </div>
</nav>