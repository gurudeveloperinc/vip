<html>
<head>
    <title>VIP Payment Page</title>
	<meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <link href="{{url('mobile/css/materialize.min.css')}}" rel="stylesheet">
    <script src="{{url('mobile/js/jquery.js')}}"></script>
    <script src="{{url('mobile/js/materialize.min.js')}}"></script>
</head>
<body>

<div align="center" class="logoDiv">
    <img src="{{url('img/logo.png')}}" alt="Thamani" height="100" class="logo">
</div>

<style>
    .header{
        text-align: center;
    }
    nav{
        background: #F23333;
    }
    body{
        padding-bottom: 30px;
    }
</style>
<nav class="nav-extended">
    <div class="nav-wrapper">
        <a href="#" class="brand-logo">
        </a>

        <h5 class="header">BUS SCHEDULE</h5>

        <ul class="tabs tabs-transparent">
            <li class="tab"><a class="active" href="#flights">DETAILS</a></li>

        </ul>
    </div>
</nav>


<div class="main-content container">
    <div class="row">
        <div class="col s12" style="padding: 0;">
            <table class="striped centered">
                <thead>
                <tr>
                    <th>Schedule</th>
                    <th>Departure</th>
                    <th>Price (GHC)</th>
                    <th>Seat Number</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                @foreach($schedules as $item)


                    <tr>
                        <td>{{$item->departureLocation}} to {{$item->arrivalLocation}}</td>
                        <td>{{$item->departureDate}} <br> {{$item->departureTime}}</td>

                        <td>{{$item->price}}</td>
                        <td>
                            <select  class="seatno browser-default">
                                <option selected disabled>Choose</option>
                                @foreach($available as $schedule)
                                    @if($schedule->shid == $item->shid)
                                        <option value="{{$schedule->sid}}">{{$schedule->seatno}}</option>
                                    @endif
                                @endforeach

                            </select>
                        </td>
                        <td><a data-url="{{url('book/' . $item->shid . '/'  )}}" class="book btn btn-teal">Book</a> </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        var book = $('.book');
        var seatno = $('.seatno');

        seatno.on('change', function () {
            var index = seatno.index(this);
            var url = book.eq(index).data('url');


            console.log(seatno.index(this));
            book.eq(index).attr('href', url + "/" + $(this).val());

        });

    })
</script>

</body>
</html>
