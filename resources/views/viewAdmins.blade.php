@extends('layouts.app')

@section('content')

    @include('nav')
    <div class="main-content container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    @if( Session::has('success') )
                        <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
                    @endif


                    @if( Session::has('error') )
                        <div class="alert alert-error" align="center">{{Session::get('error')}}</div>
                    @endif

                    <div class="panel-heading">Admin List
                        <div class="tools"><span class="icon s7-cloud-download">There are a total of {{count($admins)}} admins on the system</span><span class="icon s7-edit"></span></div>
                    </div>
                    <div class="panel-body">
                        <table id="table1" class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Registered On</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $count = 1; ?>
                            @foreach($admins as $item)

                                <tr>
                                    <td>{{$count}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->email}}</td>
                                    <td>{{$item->created_at}}</td>
                                    <td>
                                        <a href="{{url('delete-admin/' . $item->id)}}" class="btn btn-danger">Delete</a>
                                    </td>
                                </tr>
                                <?php $count++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection