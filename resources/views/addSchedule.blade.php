@extends('layouts.app')

@section('content')

    @include('nav')

    <div class="main-content container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    @if( Session::has('success') )
                        <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
                    @endif


                    @if( Session::has('error') )
                        <div class="alert alert-error" align="center">{{Session::get('error')}}</div>
                    @endif

                    <div class="panel-heading panel-heading-divider">Transport Schedule<span class="panel-subtitle">Enter a schedule</span></div>
                    <div class="panel-body">
                        <form method="post" action="{{url('add-schedule')}}">
                            {{csrf_field()}}

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Departure Location</label>
                                <select name="departureLocation" >
                                    <option disabled selected>Choose an option</option>
                                    @foreach($locations as $item)
                                        <option>{{$item->name}}</option>
                                    @endforeach
                                </select>


                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Departure Time</label>
                                <div class="col-sm-6">
                                    <input type="text" required="" placeholder="" name="departureTime" class="form-control time">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Departure Date</label>
                                <div class="col-sm-6">
                                    <input type="text" required="" name="departureDate" placeholder="2/2/12" class="form-control">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Arrival Location</label>
                                <select name="arrivalLocation" >
                                    <option disabled selected>Choose an option</option>
                                    @foreach($locations as $item)
                                        <option>{{$item->name}}</option>
                                    @endforeach
                                </select>

                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Arrival Time</label>
                                <div class="col-sm-6">
                                    <input type="text" required="" placeholder="" name="arrivalTime" class="form-control time">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Arrival Date</label>
                                <div class="col-sm-6">
                                    <input type="text" required="" placeholder="7/3/17" name="arrivalDate" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Price (GHC)</label>
                                <div class="col-sm-6">
                                    <input type="text" required="" pattern="[0-9]+" name="price" class="form-control">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Bus</label>

                                <select name="busid" >
                                    <option disabled selected>Choose an option</option>
                                    @foreach($buses as $item)
                                        <option value="{{$item->busid}}">{{$item->name}} - {{$item->regno}} - {{$item->capacity}} seats</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="form-group row">
                                <div class="col-sm-12" align="center">
                                    <button type="submit" class="btn btn-space btn-success">Submit</button>
                                    <button class="btn btn-space btn-primary">Cancel</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection