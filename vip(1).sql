-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 18, 2017 at 04:14 PM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vip`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE IF NOT EXISTS `bookings` (
  `bid` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `sname` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `reference` varchar(10) NOT NULL,
  `transactionid` varchar(255) DEFAULT NULL,
  `sid` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`bid`, `fname`, `sname`, `phone`, `email`, `reference`, `transactionid`, `sid`, `created_at`, `updated_at`) VALUES
(1, 'Toby', 'Okeke', '023094432', 'toby.okeke@gmail.com', '5bsGbL', NULL, 13, '2017-04-18 11:49:39', '2017-04-18 11:49:39'),
(2, 'Toby', 'Okeke', '023094432', 'toby.okeke@gmail.com', 'KIHtrs', NULL, 13, '2017-04-18 12:07:51', '2017-04-18 12:07:51'),
(3, 'Toby', 'St Patrick', '8081080283', 'tpatrick@gmail.com', 'MKSYBYZP', '765167890987', 15, '2017-04-18 13:26:05', '2017-04-18 12:26:05'),
(4, 'ben', 'asare', '0269527587', 'ben@gmail.com', 'lcnCDPU2', '1234567', 10, '2017-04-18 14:13:09', '2017-04-18 13:13:09');

-- --------------------------------------------------------

--
-- Table structure for table `buses`
--

CREATE TABLE IF NOT EXISTS `buses` (
  `busid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `regno` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `capacity` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buses`
--

INSERT INTO `buses` (`busid`, `name`, `regno`, `model`, `capacity`, `created_at`, `updated_at`) VALUES
(1, 'First Bus', 'GH-001', 'Xiomi', 48, '2017-04-15 15:36:32', '2017-04-15 15:36:32'),
(2, 'Second bus', 'GH-002', 'Ghana', 32, '2017-04-15 15:58:11', '2017-04-15 15:58:11');

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE IF NOT EXISTS `schedules` (
  `shid` int(11) NOT NULL,
  `departureLocation` varchar(255) NOT NULL,
  `departureTime` varchar(255) NOT NULL,
  `departureDate` varchar(255) NOT NULL,
  `arrivalLocation` varchar(255) NOT NULL,
  `arrivalTime` varchar(255) NOT NULL,
  `arrivalDate` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `busid` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`shid`, `departureLocation`, `departureTime`, `departureDate`, `arrivalLocation`, `arrivalTime`, `arrivalDate`, `price`, `busid`, `created_at`, `updated_at`) VALUES
(2, 'Accra', '6:30am', '22/4/17', 'Takoradi', '12:00pm', '22/4/17', 30, 1, '2017-04-17 19:39:54', '2017-04-15 16:43:46');

-- --------------------------------------------------------

--
-- Table structure for table `seats`
--

CREATE TABLE IF NOT EXISTS `seats` (
  `sid` int(11) NOT NULL,
  `seatno` int(11) NOT NULL,
  `shid` int(11) NOT NULL,
  `status` enum('Available','Booked') NOT NULL DEFAULT 'Available',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seats`
--

INSERT INTO `seats` (`sid`, `seatno`, `shid`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 'Available', '2017-04-15 16:43:46', '2017-04-15 16:43:46'),
(2, 2, 2, 'Available', '2017-04-15 16:43:46', '2017-04-15 16:43:46'),
(3, 3, 2, 'Available', '2017-04-15 16:43:46', '2017-04-15 16:43:46'),
(4, 4, 2, 'Available', '2017-04-18 12:12:05', '2017-04-15 16:43:46'),
(5, 5, 2, 'Available', '2017-04-15 16:43:46', '2017-04-15 16:43:46'),
(6, 6, 2, 'Available', '2017-04-15 16:43:46', '2017-04-15 16:43:46'),
(7, 7, 2, 'Available', '2017-04-15 16:43:46', '2017-04-15 16:43:46'),
(8, 8, 2, 'Available', '2017-04-15 16:43:46', '2017-04-15 16:43:46'),
(9, 9, 2, 'Available', '2017-04-15 16:43:46', '2017-04-15 16:43:46'),
(10, 10, 2, 'Booked', '2017-04-18 14:13:09', '2017-04-18 13:13:09'),
(11, 11, 2, 'Available', '2017-04-15 16:43:46', '2017-04-15 16:43:46'),
(12, 12, 2, 'Available', '2017-04-15 16:43:46', '2017-04-15 16:43:46'),
(13, 13, 2, 'Available', '2017-04-15 16:43:46', '2017-04-15 16:43:46'),
(14, 14, 2, 'Available', '2017-04-15 16:43:46', '2017-04-15 16:43:46'),
(15, 15, 2, 'Booked', '2017-04-18 13:26:05', '2017-04-18 12:26:05'),
(16, 16, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(17, 17, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(18, 18, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(19, 19, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(20, 20, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(21, 21, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(22, 22, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(23, 23, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(24, 24, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(25, 25, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(26, 26, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(27, 27, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(28, 28, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(29, 29, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(30, 30, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(31, 31, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(32, 32, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(33, 33, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(34, 34, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(35, 35, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(36, 36, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(37, 37, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(38, 38, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(39, 39, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(40, 40, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(41, 41, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(42, 42, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(43, 43, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(44, 44, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(45, 45, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(46, 46, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(47, 47, 2, 'Available', '2017-04-15 16:43:47', '2017-04-15 16:43:47'),
(48, 48, 2, 'Available', '2017-04-15 16:43:48', '2017-04-15 16:43:48');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Toby Okeke', 'toby.okeke@gmail.com', '$2y$10$fE8vX4MEhVaWbwbeZH5K1OIXtpthfnI83gv8peOt0n9y4T67gTHTW', '7fb53nWEA0Dr7pCQfaE6xxe0BqxQrGjiYcZ7D5B1LNrVJwbowSgo3pc7uQM4', '2017-04-15 14:50:12', '2017-04-15 14:50:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`bid`);

--
-- Indexes for table `buses`
--
ALTER TABLE `buses`
  ADD PRIMARY KEY (`busid`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`shid`);

--
-- Indexes for table `seats`
--
ALTER TABLE `seats`
  ADD PRIMARY KEY (`sid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `bid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `buses`
--
ALTER TABLE `buses`
  MODIFY `busid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `shid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `seats`
--
ALTER TABLE `seats`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
