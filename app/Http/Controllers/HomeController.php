<?php

namespace App\Http\Controllers;

use App\booking;
use App\bus;
use App\customer;
use App\location;
use App\schedule;
use App\seat;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;
use MPower;
use MPower_Checkout_Invoice;
use MPower_Checkout_Store;
use MPower_Setup;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request)
    {

	    $path = $request->path();
        return view('home',['path' => $path]);
    }

	public function viewBuses(Request $request) {
		$path = $request->path();
		$buses = bus::all()->sortByDesc('created_at');
		return view('viewBuses',['path' => $path,'buses' => $buses]);
	}

	public function getAddBus( Request $request ) {
		$path = $request->path();
		return view('addBus',['path' => $path]);
	}


	public function getAddSchedule( Request $request) {
		$path = $request->path();
		$buses = bus::all()->sortByDesc('created_at');
		$locations = location::all();
		$path = $request->path();
		return view('addSchedule',[
			'path' => $path,
			'buses' => $buses,
			'locations' => $locations
		]);
	}

	public function viewCustomers(Request $request) {
		$customers = customer::all();
		$path = $request->path();
		return view('viewCustomers',[
			'customers' => $customers,
			'path' => $path
		]);
	}

	public function deleteCustomer( Request $request , $id) {
		customer::destroy($id);
		$request->session()->flash('success', "Customer deleted.");

		return redirect('view-customers');
	}

	public function viewAdmins(Request $request) {
		$admins = User::where('role','Admin')->get();
		$path = $request->path();
		return view('viewAdmins',[
			'admins' => $admins,
			'path' => $path
		]);
	}

	public function getAddAdmin(Request $request) {
		$path = $request->path();

		return view('addAdmin',[
			'path' => $path
		]);
	}

	public function deleteAdmin( Request $request, $id ) {
		User::destroy($id);
		$request->session()->flash('success', "Admin deleted.");

		return redirect('view-admins');
	}

	public function viewSchedule( Request $request) {
		$path = $request->path();
		$schedules = schedule::all()->sortByDesc('created_at');
		return view('viewSchedule',['path' => $path,'schedules' => $schedules]);
	}

	public function viewBookings( Request $request ) {
		$path = $request->path();
		$schedules = schedule::all()->sortByDesc('created_at');
		return view('viewBookings',['path' => $path,'schedules' => $schedules]);
	}

	public function viewBookingDetails( Request $request, $sid ) {
		$customer = booking::where('sid',$sid)->first();
		$path = $request->path();

		return view('bookingDetails',[
			'customer' => $customer,
			'path' => $path
		]);
	}

	public function viewLocations(Request $request) {
		$locations = location::all();
		$path = $request->path();
		return view('viewLocations',[
			'locations' => $locations,
			'path' => $path
		]);
	}

	public function getAddLocation(Request $request) {
		$path = $request->path();
		return view('addLocation',[
			'path' => $path
		]);
	}

	public function viewBookingSeats(Request $request, $shid ) {
		$seats = seat::where('shid',$shid)->get();
		$schedule = schedule::find($shid);
		$path = $request->path();
		$available = seat::where('shid',$shid)->where('status','Available')->count();

		return view('viewSeats',[
			'path' => $path,
			'seats' => $seats,
			'schedule' => $schedule,
			'available' => $available
		]);
	}

	public function viewReports( Request $request ) {
		$path = $request->path();

		$locations = location::all();

		$schedules = schedule::all();
		$highest["number"] = 0;
		$highest["shid"] = "";

		foreach($schedules as $item){
			$number = seat::where('shid',$item->shid)->where('status','Booked')->count();
			if($highest["number"] < $number){
				$highest["number"] = $number;
				$highest["shid"] = $item->shid;
			}

		}

		$bookings = booking::all()->sortByDesc('created_at');

		if( Input::has('days') ){
		$bookings = booking::where('created_at', ">" , Carbon::now()->subDay(Input::get("days")) )->get();
		}



		return view('viewReports',[
			'path' => $path,
			'bookings' => $bookings,
			'schedules' => $schedules,
			'locations' => $locations

		]);
	}

	public function removeBooking( Request $request, $id, $shid ) {
		$booking = booking::find($id);
		$booking->transactionid = null;
		$booking->save();

		$seat = seat::find($booking->sid);
		$seat->status = 'Available';
		$seat->save();
		$request->session()->flash('success','Booking Removed');

		mail($booking->email, "Booking Payment issue for reference $booking->reference", "Dear Customer\nYour transaction ID wasn't found on our system and hence your transaction is now pending. Please re-enter valid transaction ID or contact an admin with the following number 0261332884.\nThank You");

		return redirect('view-bookings/' . $shid);
	}

	public function logout() {
		Auth::logout();
		return redirect('/');
	}

	public function postAddBus( Request $request ) {

		$bus = new bus();
		$bus->name = $request->input('name');
		$bus->regno = $request->input('regno');
		$bus->model = $request->input('model');
		$bus->capacity = $request->input('capacity');
		$status = $bus->save();

		if($status)
		$request->session()->flash('success' , 'Successfully Added Bus');
		else
		$request->session()->flash('error' , 'An error occurred. Please try again.');

		return redirect('/add-bus');
	}

	public function postAddSchedule( Request $request ) {
		$schedule = new schedule();
		$schedule->departureLocation = $request->input('departureLocation');
		$schedule->departureTime = $request->input('departureTime');
		$schedule->departureDate = $request->input('departureDate');
		$schedule->arrivalLocation = $request->input('arrivalLocation');
		$schedule->arrivalTime = $request->input('arrivalTime');
		$schedule->arrivalDate = $request->input('arrivalDate');
		$schedule->price = $request->input('price');
		$schedule->busid = $request->input('busid');
		$status = $schedule->save();

		$bus = bus::find($request->input('busid'));

		for($i = 1; $i < $bus->capacity + 1; $i++){
			$seat = new seat();

			$seat->seatno = $i;
			$seat->shid = $schedule->shid;
			$seat->save();
		}

		if($status)
		$request->session()->flash("success","Schedule Created");
		else
		$request->session()->flash('error' , 'An error occurred. Please try again.');

		return redirect('/add-schedule');
	}

	public function postAddLocation( Request $request ) {
		$location = new location();
		$location->name = $request->input('name');
		$location->save();

		$request->session()->flash('success','Location added successfully');
		return redirect('add-location');
	}

	public function postAddAdmin( Request $request ) {
		$admin = new User();
		$admin->name = $request->input('name');
		$admin->email = $request->input('email');
		$admin->password = bcrypt($request->input('password'));
		$status = $admin->save();

		if($status)
		$request->session()->flash('success',"Admin added successfully");
		else
		$request->session()->flash('error',"Sorry an error occurred. Please try again");

		return redirect('add-admin');
	}


}
