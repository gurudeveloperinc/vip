<?php

namespace App\Http\Controllers;

use App\booking;
use App\customer;
use App\schedule;
use App\seat;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use MPower_Checkout_Invoice;
use MPower_Checkout_Store;
use MPower_Setup;

class PublicController extends Controller
{
	public function index() {
		return view('welcome');
    }

	public function pay( Request $request ) {
		$ref = $request->input('reference');
		$booking = new booking();
		$booking->fname = $request->input('fname');
		$booking->sname = $request->input('sname');
		$booking->email = $request->input('email');
		$booking->phone = $request->input('phone');
		$booking->reference =  $ref;
		$booking->sid = $request->input('seat');
		$booking->save();

		$request->session()->flash("success", "Booking reservation made for reference - $ref");

		return redirect('bus-schedule');
	}

	public function getConfirm() {
		return view('addTransactionID');
	}

	public function confirm( Request $request ) {
		try {
			$booking                = booking::where( 'reference', $request->input( 'reference' ) )->first();
			$booking->transactionid = $request->input( 'transactionid' );
			$booking->save();

			$seat         = seat::find( $booking->sid );
			$seat->status = "Booked";
			$seat->save();


			mail( $booking->email, "Your booking has been confirmed- $booking->reference", "Thank you for using VIP Bus Transport." );

			$request->session()->flash( "success", "Booking Confirmed" );
			return redirect('bus-schedule');
		}
		 catch(Exception $e){
			 $request->session()->flash( "error", "Booking reference invalid" );
			 return redirect('bus-schedule');
		 }

	}

	public function mpowerPay(Request $request) {

		$schedule = schedule::find($request->input('schedule'));
		$seat = $request->input('seat');
		$price = $schedule->price;

		MPower_Setup::setMasterKey("dd6f2c90-f075-012f-5b69-00155d866600");
		MPower_Setup::setPublicKey("test_public_oDLVlm1eNyh0IsetdhdJvcl0ygA");
		MPower_Setup::setPrivateKey("test_private_zzF3ywvX9DE-OSDNhUqKoaTI4wc");
		MPower_Setup::setMode("test");
		MPower_Setup::setToken("ca03737cf942cf644f36");

		//Setup your Store information
		MPower_Checkout_Store::setName("VIP Bus Transport");
		MPower_Checkout_Store::setTagline("Safe, fast, reliable");
		MPower_Checkout_Store::setPhoneNumber("0231234567");
		MPower_Checkout_Store::setPostalAddress("Post office Box AN 10604");

		$invoice = new MPower_Checkout_Invoice();

		/*
		   Adding items to your invoice is very basic, the parameters expected are
		   name_of_item, quantity, unit_price, total_price and optional item
		   description.
		*/


		/* You can optionally set a general invoice description text which can
		be used in cases where your invoice does not need an items list or in cases
		where you need to include some extra descriptive information to your invoice */
		$invoice->setDescription("Vip Bus Ticket\n Payment for seat $seat");


		$invoice->setTotalAmount($price);

		// The code below depicts how to create the checkout invoice on our servers
		// and redirect to the checkout page.
		if($invoice->create()) {
			header("Location: ".$invoice->getInvoiceUrl());
		}else{
			echo $invoice->response_text;
		}


		MPower_Checkout_Store::setCancelUrl("http://vip.gurudeveloperinc.com/cancel");

		MPower_Checkout_Store::setReturnUrl("http://vip.gurudeveloperinc.com/confirm"); // confirm url


	}

	public function mpowerConfirm() {
		// MPower will automatically set the confirm token to QUERY_STRING
		// param $_GET['token'] if not explicitly specified
		$token = $_GET["token"];

		$invoice = new MPower_Checkout_Invoice();
		if ($invoice->confirm($token)) {

			// Retrieving Invoice Status
			// Status can be either completed, pending, canceled, fail
			print $invoice->getStatus();

			// You can retrieve the Name, Email & Phone Number
			// of the customer using the callbacks below
			print $invoice->getCustomerInfo('name');
			print $invoice->getCustomerInfo('email');
			print $invoice->getCustomerInfo('phone');

			// Return the URL to the Invoice Receipt PDF file for download
			print $invoice->getReceiptUrl();

			// Retrieving any custom data you have added to the invoice
			// Please remember to use the right keys you used to set them earlier
			print $invoice->getCustomData("Firstname");
			print $invoice->getCustomData("CartId");
			print $invoice->getCustomData("Plan");

			// You can also callback the total amount set earlier
			print $invoice->getTotalAmount();

		}else{
			print $invoice->getStatus();
			print $invoice->response_text;
			print $invoice->response_code;
		}
	}

	public function getPayment( $shid, $seat ) {
		$schedule = schedule::find($shid);
		$seat = seat::find($seat);
		$reference = Str::random(8);

		return view('payment',[
			'schedule' => $schedule,
			'seat' => $seat,
			'path' => '',
			'reference' => $reference
		]);
	}

	public function getSchedule() {
		$schedules = schedule::all()->sortByDesc('created_at');
		return view('busSchedule',['schedules' => $schedules]);
	}

	public function bookTicket() {

		$available = seat::all()->where('status','Available');
		$schedules = schedule::all()->sortByDesc('created_at');
		return view('bookTicket',[
			'schedules' => $schedules,
			'available' => $available
		]);
	}

	public function appLogin( Request $request ) {
		$customer = customer::where('email',$request->input('email'))->where('password',$request->input('password'))->first();


		if (!empty($customer)){
			echo $customer;
		} else{
			echo 0;
		}

	}

	public function appRegister( Request $request ) {
		$customer = new customer();
		$customer->fname = $request->input('fname');
		$customer->sname = $request->input('sname');
		$customer->email = $request->input('email');
		$customer->phone = $request->input('phone');
		$customer->password = $request->input('password');
		$status = $customer->save();

		if($status) echo 1;
		else echo 0;
	}

	public function getCancelTicket() {
		return view('cancelTicket');
	}

	public function postCancelTicket( Request $request ) {
		mail("admin@vipapp.com","Cancel Request", "Good day, A cancellation request was initiated for reference number $request->input('reference')");

		$request->session()->flash("success","Cancellation request initiated");
		return redirect('cancel-ticket');
	}


}
