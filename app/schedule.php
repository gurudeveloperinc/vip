<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class schedule extends Model
{
    protected $primaryKey = 'shid';

	public function Bus() {
		return $this->hasOne('App\bus','busid','busid');
	}
}
