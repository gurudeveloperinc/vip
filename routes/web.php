<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('view-buses','HomeController@viewBuses');
Route::get('add-bus','HomeController@getAddBus');
Route::get('view-schedule','HomeController@viewSchedule');
Route::get('add-schedule','HomeController@getAddSchedule');
Route::get('view-bookings','HomeController@viewBookings');
Route::get('view-bookings/{shid}','HomeController@viewBookingSeats');
Route::get('booking-details/{sid}','HomeController@viewBookingDetails');
Route::get('view-locations','HomeController@viewLocations');
Route::get('add-location','HomeController@getAddLocation');
Route::get('add-admin','HomeController@getAddAdmin');
Route::get('view-admins','HomeController@viewAdmins');
Route::get('view-customers','HomeController@viewCustomers');
Route::get('delete-admin/{id}','HomeController@deleteAdmin');
Route::get('delete-customer/{id}','HomeController@deleteCustomer');
Route::get('view-reports','HomeController@viewReports');
Route::get('remove-booking/{id}/{shid}','HomeController@removeBooking');
Route::get('logout','HomeController@logout');

Route::post('add-bus','HomeController@postAddBus');
Route::post('add-schedule','HomeController@postAddSchedule');
Route::post('add-location','HomeController@postAddLocation');
Route::post('add-admin','HomeController@postAddAdmin');

Route::get('book/{shid}/{seat}','PublicController@getPayment');
//Route::get('confirm','PublicController@confirm');


Route::get('bus-schedule','PublicController@getSchedule');
Route::get('book-ticket','PublicController@bookTicket');
Route::get('confirm','PublicController@getConfirm');
Route::get('cancel-ticket','PublicController@getCancelTicket');


Route::post('app-login','PublicController@appLogin');
Route::post('app-register','PublicController@appRegister');
Route::post('confirm','PublicController@confirm');
Route::post('cancel-ticket','PublicController@postCancelTicket');

Route::post('pay','PublicController@pay');


